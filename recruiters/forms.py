from django import forms
from .models import Job


class NewJobForm(forms.ModelForm):
    class Meta:
        model = Job
        fields = ['title', 'company', 'location',
                  'description', 'skills_req', 'job_type', 'link']
        help_texts = {
            'title': 'Название',
            'company': 'Компания',
            'location': 'Нахождение',
            'description': 'Описание' ,
            'skills_req': 'Необходимые навыки',
            'job_type': 'Тип работы',
            'link': "Ссылка"
        }


class JobUpdateForm(forms.ModelForm):
    class Meta:
        model = Job
        fields = ['title', 'company', 'location',
                  'description', 'skills_req', 'job_type', 'link']
        help_texts = {
            'title': 'Название',
            'company': 'Компания',
            'location': 'Нахождение',
            'description': 'Описание' ,
            'skills_req': 'Необходимые навыки',
            'job_type': 'Тип работы',
            'link': "Ссылка"
        }
